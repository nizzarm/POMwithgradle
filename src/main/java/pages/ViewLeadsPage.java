package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class ViewLeadsPage extends  ProjectMethods {

	public void EditButton(){
		WebElement clickedit = locateElement("linktext", "Edit");
		click(clickedit);
	}
	
}
