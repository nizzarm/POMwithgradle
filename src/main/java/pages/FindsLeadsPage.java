package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class FindsLeadsPage extends ProjectMethods {
	
	public ViewLeadsPage FindLeads() {
	WebElement elephonenumber = locateElement("xpath","//span[text()='Phone']");
	click(elephonenumber);
	WebElement typephnum = locateElement("name","phoneNumber");
	type(typephnum,"9944198408");
	WebElement buttonfindleads = locateElement("xpath", "//button[text()='Find Leads']");
	click(buttonfindleads);
	WebElement data1 = locateElement("xpath","(//div[@class=\"x-grid3-cell-inner x-grid3-col-partyId\"]/a)[1]");
	click(data1);
	return new ViewLeadsPage();
	}
}
